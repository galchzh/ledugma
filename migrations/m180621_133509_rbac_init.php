<?php

use yii\db\Migration;

/**
 * Class m180621_133509_rbac_init
 */
class m180621_133509_rbac_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        
        // add "author" role and give this role the "createPost" permission
        $author = $auth->createRole('author');
        $auth->add($author);

        $editor = $auth->createRole('editor');
        $auth->add($editor);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        
        $auth->addChild($admin, $editor);
        $auth->addChild($editor,$author);

        $manageUsers = $auth->createPermission('manageUsers');
        $auth->add($manageUsers);

        $createPost = $auth->createPermission('createPost');
        $auth->add($createPost);

        $updatePost = $auth->createPermission('updatePost');
        $auth->add($updatePost);

        $publishPost = $auth->createPermission('publishPost');
        $auth->add($publishPost);                
        
        $deletePost = $auth->createPermission('deletePost');
        $auth->add($deletePost);                  
        
        $updateOwnPost = $auth->createPermission('updateOwnPost');

        $rule = new \app\rbac\AuthorRule;
        $auth->add($rule);
        
        $updateOwnPost->ruleName = $rule->name;                 
        $auth->add($updateOwnPost);                                                    
        
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($author, $createPost);
        $auth->addChild($editor, $updatePost);
        $auth->addChild($editor, $publishPost); 
        $auth->addChild($editor, $deletePost);
        $auth->addChild($updateOwnPost, $updatePost); 
        $auth->addChild($author, $updateOwnPost);  
                   
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180621_133509_rbac_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_133509_rbac_init cannot be reverted.\n";

        return false;
    }
    */
}
